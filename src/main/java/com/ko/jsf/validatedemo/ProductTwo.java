package com.ko.jsf.validatedemo;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProductTwo {

	// fields
	private String productName;
	private String rating;
	private String originDistrictPostalCode;

	// no-arg constructor
	public ProductTwo() {

	}

	// getters and setters
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getOriginDistrictPostalCode() {
		return originDistrictPostalCode;
	}

	public void setOriginDistrictPostalCode(String originDistrictPostalCode) {
		this.originDistrictPostalCode = originDistrictPostalCode;
	}

}
