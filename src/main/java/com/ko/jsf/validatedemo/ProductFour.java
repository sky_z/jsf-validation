package com.ko.jsf.validatedemo;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

@ManagedBean
public class ProductFour {

	// fields
	private String productName;
	private String productCode;

	// no-arg constructor
	public ProductFour() {

	}

	// getter and setter
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public void validateTheProductCode(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		if (value == null) {
			return;
		}

		String data = value.toString();

		// Product coude must start with KO
		if (!data.startsWith("KO")) {

			FacesMessage message = new FacesMessage("Productcode must start with KO");

			throw new ValidatorException(message);

		}
	}

}
