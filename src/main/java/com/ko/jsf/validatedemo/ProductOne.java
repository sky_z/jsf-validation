package com.ko.jsf.validatedemo;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProductOne {

	// fields
	private String name;
	private String optionalName;
	private String category;

	// no-arg constructor
	public ProductOne() {

	}

	// getter and setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOptionalName() {
		return optionalName;
	}

	public void setOptionalName(String optionalName) {
		this.optionalName = optionalName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
