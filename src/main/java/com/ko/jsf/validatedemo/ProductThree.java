package com.ko.jsf.validatedemo;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProductThree {

	// fields
	private String productName;
	private String originDistrict;

	// no-arg constructor
	public ProductThree() {

	}

	// getter and setter
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getOriginDistrict() {
		return originDistrict;
	}

	public void setOriginDistrict(String originDistrict) {
		this.originDistrict = originDistrict;
	}

}
